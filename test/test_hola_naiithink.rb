require 'minitest/autorun'

require 'hola_naiithink'

class HolaTest < Minitest::Test
    def test_say_hello
        out, err = capture_io { Hola.hi }
        assert_match %r{^Hello world!(\n|\r\n)$}, out, "stdout: #{out}"
        assert_match %r{^$}, err, "stderr: #{err}"
    end
end
