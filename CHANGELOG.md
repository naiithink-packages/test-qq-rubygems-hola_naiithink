# hola_naiithink changelog

| Version | Description |
| :-----: | :---------- |
| `v0.1.0` | Check out that `Rakefile`! |
| `v0.0.1` | Resolve the homepage domain name. |
| `v0.0.0` | The initial `Hola` class is released. |
