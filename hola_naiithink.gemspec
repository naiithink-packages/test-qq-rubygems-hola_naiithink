=begin
https://guides.rubygems.org/specification-reference/
=end

Gem::Specification.new do |s|
    s.name = 'hola_naiithink'
    s.version = '0.1.0'
    s.summary = "Hola!"
    s.description = "A simple hello world gem"
    s.author = ["naiithink"]
    s.email = 'helloworld@catlover.com'
    s.files = ["lib/hola_naiithink.rb"]
    s.homepage =
        'https://naiithink-packages.gitlab.io/test-qq-rubygems-hola_naiithink'
    s.license = 'GPL-3.0'
end
